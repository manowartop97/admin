<?php

use Illuminate\Database\Seeder;

class AdminsDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Admin\Admin::query()->create([
           'email' => 'admin@admin.admin',
           'password' => Hash::make('123456')
        ]);
    }
}
