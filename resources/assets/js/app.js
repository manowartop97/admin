/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
import {ErrorsHandler} from "./services/error/ErrorsHandler";


require('./bootstrap');

window.Vue = require('vue');

import {router} from './router/routes';
import {store} from './vuex/store';
import Buefy from 'buefy';
import 'buefy/dist/buefy.css';
import 'material-design-icons-iconfont/dist/material-design-icons.css';
import '@fortawesome/fontawesome-free/css/all.css';
import Vue from 'vue';
import axios from 'axios';
import VueAxios from 'vue-axios';
import VeeValidate from 'vee-validate';
import {Validator} from 'vee-validate';
import VueSimpleContextMenu from 'vue-simple-context-menu';

Vue.use(VueAxios, axios);
Vue.use(VeeValidate, {events: 'blur'});
Vue.use(Buefy);
Vue.use(require('vue-shortkey'));

require('./components');
require('./validation/disctionary');

Vue.component('Validator', Validator);
Vue.component('vue-simple-context-menu', VueSimpleContextMenu);

const app = new Vue({
    el: '#app',
    data() {
        return {
            errorsHandler: new ErrorsHandler(this)
        }
    },
    created() {
        if (this.$store.getters.isAuthenticated) {
            this.$store.dispatch('userRequest');
        }
    },
    methods: {
        showToast(type, message, position = 'is-top-right', duration = 3000) {
            this.$toast.open({
                duration: duration,
                message: message,
                position: position,
                type: type
            })
        }
    },
    Validator,
    store,
    router,
});
