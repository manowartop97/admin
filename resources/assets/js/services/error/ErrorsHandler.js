/**
 *
 */
class ErrorsHandler {

    /**
     * @param root
     */
    constructor(root) {
        this.root = root;
    }

    /**
     * Errors handler
     * @param error
     * @param errorsBag
     */
    handleErrors(error, errorsBag = null) {
        let code = typeof error.code !== 'undefined' ? error.code : error.status_code;

        switch (code) {
            case 400:
                this.root.showToast('is-danger', error.message);
                break;
            case 403:
                this.root.showToast('is-danger', error.message);
                this.root.$router.push('/403');
                break;
            case 404:
                this.root.$router.push('/403');
                break;
            case 422:
                errorsBag.clear();
                $.each(error.errors, function (field, errors) {
                    errorsBag.add({field: field, msg: errors[0]});
                });
                break;
            case 500:
                this.root.showToast('is-danger', error.message);
                break;
        }
    }
}

export {ErrorsHandler}
