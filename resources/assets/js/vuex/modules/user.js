import Vue from 'vue'

const USER_STATUS_ACTIVE = 4;

const state = {
    status: '',
    profile: {}
};

const getters = {
    getProfile: state => state.profile,
    isProfileLoaded: state => !!state.profile.name,
}

const actions = {
    userRequest: ({commit, dispatch}) => {
        commit('userRequest');
        axios.get('/api/v1/auth/user')
            .then((resp) => {
                commit('userSuccess', resp.data.data);
            })
            .catch((err) => {
                commit('userError');
                dispatch('authLogout')
            })
    },
}

const mutations = {
    userRequest: (state) => {
        state.status = 'loading';
    },
    userSuccess: (state, resp) => {
        state.status = 'success';
        Vue.set(state, 'profile', resp);
    },
    userError: (state) => {
        state.status = 'error';
    }
}

export default {
    state,
    getters,
    actions,
    mutations,
}
