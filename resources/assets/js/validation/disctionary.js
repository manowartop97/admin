import {Validator} from 'vee-validate';

const dictionary = {
    ru: {
        messages: {
            required: () => 'Поле обязательно к заполнению.',
            email: () => 'Введите валидный email адрес.',
            confirmed: () => 'Пароль и его подтверждение не совпадают.',
            min: () => 'Слишком короткое значение'
        }
    }
};

Validator.localize(dictionary);
Validator.localize('ru');