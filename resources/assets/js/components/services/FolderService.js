import axios from 'axios';

class FolderService {

    /**
     *
     */
    constructor() {
        this.endpoint = '/api/v1/file-manager/folders';
    }

    /**
     * Get folders
     * @returns {*}
     */
    getFolders() {
        return axios.get(this.endpoint);
    }

    /**
     * Get Folders tree
     * @returns {*}
     */
    getFoldersTree() {
        return axios.get(this.endpoint + '/tree');
    }

    /**
     * Create folder method
     * @param data
     * @returns {Promise<AxiosResponse<T>>}
     */
    createFolder(data) {
        return axios.post(this.endpoint, data);
    }

    /**
     * Update folder Component
     * @param id
     * @param data
     * @returns {Promise<AxiosResponse<T>>}
     */
    updateFolder(id, data) {
        return axios.put(this.endpoint + '/' + id, data);
    }

    /**
     * Delete folder
     * @param id
     * @returns {Promise<AxiosResponse<T>>}
     */
    deleteFolder(id) {
        return axios.delete(this.endpoint + '/' + id);
    }
}

export {FolderService};