import axios from 'axios';

class FileService {

    /**
     *
     */
    constructor() {
        this.endpoint = '/api/v1/file-manager/files';
    }

    /**
     * Rename file
     * @param id
     * @param data
     * @returns {Promise<AxiosResponse<T>>}
     */
    renameFile(id, data) {
        return axios.patch(this.endpoint + '/' + id + '/rename', data);
    }

    /**
     * Delete files
     * @param data
     */
    deleteFiles(data) {
        return axios.post(this.endpoint + '/delete', data);
    }

    /**
     * Move file(s) to another folder
     * @param data
     * @returns {Promise<AxiosResponse<T>>}
     */
    moveFiles(data) {
        return axios.post(this.endpoint + '/move', data);
    }

    /**
     * Get files
     *
     * @param searchParams
     * @returns {Promise<AxiosResponse<T>>}
     */
    getFiles(searchParams) {
        return axios.get(this.endpoint, {params: searchParams});
    }
}

export {FileService};