import Vue from 'vue';
import VueRouter from 'vue-router'
import {store} from '../vuex/store'

Vue.use(VueRouter);

const ifNotAuthenticated = (to, from, next) => {
    if (!store.getters.isAuthenticated) {
        next();
        return
    }
    next('/dashboard')
};

const ifAuthenticated = (to, from, next) => {
    if (store.getters.isAuthenticated) {
        next();
        return
    }
    next('/login')
};

import Login from './../components/auth/login';
import Layout from './../components/layout/layout';
import Dashboard from './../components/dashboard';
import Forbidden from './../components/errors/ForbiddenComponent';
import NotFound from './../components/errors/NotFoundComponent';

const routes = [
    {
        path: '/403',
        name: '403',
        component: Forbidden,
    },
    {
        path: '/404',
        name: '404',
        component: NotFound,
    },
    {
        path: '/login',
        name: 'login',
        component: Login,
        beforeEnter: ifNotAuthenticated,
    },
    {
        path: '/',
        component: Layout,
        beforeEnter: ifAuthenticated,
        children: [
            {
                path: 'dashboard',
                name: 'dashboard.index',
                component: Dashboard
            }
        ]
    },
];


export const router = new VueRouter({
    mode: 'history',
    routes
});

