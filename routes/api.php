<?php

use Dingo\Api\Routing\Router;

/** @var Router $api */
$api = app(Router::class);

$api->version('v1', ['namespace' => 'App\Api\V1\Http\Controllers', 'prefix' => 'api/v1', 'middleware' => ['bindings']], function (Router $api) {
    // Auth
    $api->group(['prefix' => 'auth', 'namespace' => 'Auth'], function (Router $api) {
        $api->post('/login', 'LoginController@login')->middleware('guest:admin');
        $api->get('/user', 'LoginController@getUser')->middleware('auth:admin');
        $api->post('/logout', 'LoginController@logout')->middleware('auth:admin');
        $api->post('/refresh', 'LoginController@refresh');
    });

    // File manager
    $api->group(['prefix' => 'file-manager', 'namespace' => 'FileManager', 'middleware' => 'auth:admin'], function (Router $api) {
        // Folders
        $api->group(['prefix' => 'folders'], function (Router $api) {
            $api->get('/tree', 'FolderController@getTree');
        });
        $api->resource('folders', 'FolderController');

        // Files
        $api->group(['prefix' => 'files'], function (Router $api) {
            $api->get('', 'FileController@index');
            $api->post('', 'FileController@create');
            $api->patch('/{file}/rename', 'FileController@rename');
            $api->post('/move', 'FileController@move');
            $api->post('/delete', 'FileController@delete');
        });
    });
});
