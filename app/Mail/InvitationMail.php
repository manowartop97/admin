<?php

namespace App\Mail;

use App\Models\User\Invitation;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

/**
 * Class InvitationMail
 * @package App\Mail
 */
class InvitationMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var Invitation
     */
    private $invitation;

    /**
     * Create a new message instance.
     *
     * @param Invitation $invitation
     */
    public function __construct(Invitation $invitation)
    {
        $this->invitation = $invitation;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.invitations.invitation-mail', [
            'user'       => $this->invitation->user,
            'invitation' => $this->invitation
        ]);
    }
}
