<?php
/**
 * Created by PhpStorm.
 * User: manowartop
 * Date: 2019-06-23
 * Time: 17:35
 */

namespace App\Repositories;

use App\Traits\SearchTrait;
use Exception;
use Illuminate\Database\Eloquent\Model;

/**
 * Class BaseRepository
 * @package App\Repositories
 *
 */
abstract class BaseRepository implements BaseRepositoryInterface
{
    use SearchTrait;

    /**
     * @var string
     */
    protected $entityClass;

    /**
     * BaseRepository constructor.
     * @throws RepositoryException
     * @throws Exception
     */
    public function __construct()
    {
        if (is_null($this->entityClass) || !class_exists($this->entityClass)) {
            throw new Exception("entityClass не определен в" . get_class($this) . ' или класс не найден');
        }
    }

    /**
     * Create model
     *
     * @param array $data
     * @return Model|null
     */
    public function create(array $data): ?Model
    {
        /** @var Model $model */
        $model = resolve($this->entityClass);

        if (!$model->fill($data)->save()) {
            return null;
        }

        return $model;
    }

    /**
     * Update model
     *
     * @param Model $model
     * @param array $data
     * @return Model|null
     */
    public function update(Model $model, array $data): ?Model
    {
        if (!$model->fill($data)->save()) {
            return null;
        }

        return $model;
    }

    /**
     * Delete model
     *
     * @param Model $model
     * @return bool
     * @throws Exception
     */
    public function delete(Model $model): bool
    {
        return !is_null($model->delete());
    }
}
