<?php
/**
 * Created by PhpStorm.
 * User: hellenmicky
 * Date: 11.07.2019
 * Time: 12:14
 */

namespace App\Repositories\FileManager\Contracts;

use App\Repositories\BaseRepositoryInterface;

/**
 * Interface FolderRepositoryInterface
 * @package App\Repositories\FileManager\Contracts
 */
interface FolderRepositoryInterface extends BaseRepositoryInterface
{

}