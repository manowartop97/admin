<?php
/**
 * Created by PhpStorm.
 * User: hellenmicky
 * Date: 15.07.2019
 * Time: 16:24
 */

namespace App\Repositories\FileManager\Contracts;

use App\Repositories\BaseRepositoryInterface;

/**
 * Interface FileRepositoryInterface
 * @package App\Repositories\FileManager\Contracts
 */
interface FileRepositoryInterface extends BaseRepositoryInterface
{

}