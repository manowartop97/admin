<?php
/**
 * Created by PhpStorm.
 * User: manowartop
 * Date: 2019-07-09
 * Time: 22:11
 */

namespace App\Repositories\FileManager;

use App\Models\FileManager\Folder;
use App\Repositories\BaseRepository;
use App\Repositories\FileManager\Contracts\FolderRepositoryInterface;

/**
 * Class FolderRepository
 * @package App\Repositories\FileManager
 */
class FolderRepository extends BaseRepository implements FolderRepositoryInterface
{
    /**
     * @var string
     */
    protected $entityClass = Folder::class;

    /**
     * @var array
     */
    protected $filterableParams = [];
}
