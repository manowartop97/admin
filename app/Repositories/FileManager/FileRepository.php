<?php
/**
 * Created by PhpStorm.
 * User: manowartop
 * Date: 2019-07-09
 * Time: 22:10
 */

namespace App\Repositories\FileManager;

use App\Models\FileManager\File;
use App\Repositories\BaseRepository;
use App\Repositories\FileManager\Contracts\FileRepositoryInterface;
use Cache;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Psr\SimpleCache\InvalidArgumentException;

/**
 * Class FileRepository
 * @package App\Repositories\FileManager
 */
class FileRepository extends BaseRepository implements FileRepositoryInterface
{
    /**
     * @var string
     */
    protected $entityClass = File::class;

    /**
     * @var int
     */
    protected $cacheTtl = 60;

    /**
     * @var array
     */
    protected $filterableParams = [
        'folder_id' => [
            'operator' => '='
        ],
        'id'        => [
            'operator' => 'in'
        ]
    ];

    /**
     * @var array
     */
    protected $orders = [
        'id' => 'desc'
    ];

    /**
     * Get filtered and cached collection
     *
     * @param array $search
     * @return Collection
     * @throws Exception
     */
    public function getFilteredCollection(array $search = []): Collection
    {
        if (!isset($search['folder_id'])) {
            return parent::getFilteredCollection($search);
        }

        return Cache::remember($this->entityClass . $search['folder_id'], $this->cacheTtl, function () use ($search) {
            return $this->getFilteredQuery($search)->get();
        });
    }

    /**
     * Create model
     *
     * @param array $data
     * @return Model|null
     * @throws InvalidArgumentException
     */
    public function create(array $data): ?Model
    {
        Cache::delete($this->entityClass . $data['folder_id']);

        return parent::create($data);
    }

    /**
     * Update model
     *
     * @param Model|File $model
     * @param array $data
     * @return Model|null
     * @throws InvalidArgumentException
     */
    public function update(Model $model, array $data): ?Model
    {
        Cache::delete($this->entityClass . $model->folder_id);

        if (isset($data['folder_id'])) {
            Cache::delete($this->entityClass . $data['folder_id']);
        }

        return parent::update($model, $data);
    }

    /**
     * Delete model
     *
     * @param Model|File $model
     * @return bool
     * @throws InvalidArgumentException
     * @throws Exception
     */
    public function delete(Model $model): bool
    {
        Cache::delete($this->entityClass . $model->folder_id);

        return parent::delete($model);
    }
}
