<?php
/**
 * Created by PhpStorm.
 * User: hellenmicky
 * Date: 11.07.2019
 * Time: 12:13
 */

namespace App\Repositories;

use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class BaseRepositoryInterface
 * @package App\Repositories
 *
 * @method Collection getFilteredCollection(array $search = [])
 */
interface BaseRepositoryInterface
{
    /**
     * Create model
     *
     * @param array $data
     * @return Model|null
     */
    public function create(array $data): ?Model;

    /**
     * Update model
     *
     * @param Model $model
     * @param array $data
     * @return Model|null
     */
    public function update(Model $model, array $data): ?Model;

    /**
     * Delete model
     *
     * @param Model $model
     * @return bool
     * @throws Exception
     */
    public function delete(Model $model): bool;
}