<?php

namespace App\Observers\FileManager;

use App\Models\FileManager\Folder;
use App\Services\Auth\LoginService;
use App\Services\FileManager\StorageFolderService;
use Auth;

/**
 * Class FolderObserver
 * @package App\Observers\FileManager
 */
class FolderObserver
{
    /**
     * @var StorageFolderService
     */
    protected $storageFolderService;

    /**
     * FolderObserver constructor.
     * @param StorageFolderService $storageFolderService
     */
    public function __construct(StorageFolderService $storageFolderService)
    {
        $this->storageFolderService = $storageFolderService;
    }

    /**
     * @param Folder $folder
     * @return void
     */
    public function creating(Folder $folder): void
    {
        $folder->admin_id = Auth::guard(LoginService::ADMIN_GUARD)->id();
    }
}
