<?php

namespace App\Observers\FileManager;

use App\Models\FileManager\File;
use App\Services\Auth\LoginService;
use Auth;

/**
 * Class FileObserver
 * @package App\Observers\FileManager
 */
class FileObserver
{
    /**
     * @param File $file
     * @return void
     */
    public function creating(File $file): void
    {
        $file->admin_id = Auth::guard(LoginService::ADMIN_GUARD)->id();
        $file->name = $this->generateUniqueFileName($file->name, $file->folder_id, $file->extension);
    }

    /**
     * @param File $file
     * @return void
     */
    public function updating(File $file): void
    {
        $file->name = $this->generateUniqueFileName($file->name, $file->folder_id, $file->extension, $file->id);
    }

    /**
     * Append (1),(2)..(n) to file name if such file exists in dir
     *
     * @param string $filename
     * @param int $folderId
     * @param string $extension
     * @param int|null $id
     * @return string
     */
    protected function generateUniqueFileName(string $filename, int $folderId, string $extension, int $id = null): string
    {
        $iteration = 1;

        if (!preg_match("/.{$extension}$/ui", $filename)) {
            $filename .= ".{$extension}";
        }

        while ($this->isFileNameExists($filename, $folderId, $id)) {

            $filename = str_replace('.' . $extension, '', $filename);

            $previousIteration = $iteration - 1;

            if ($previousIteration < 1) {
                $filename .= "({$iteration})";
            } else {
                $filename = str_replace("({$previousIteration})", "({$iteration})", $filename);
            }

            $filename .= ".{$extension}";
            $iteration++;
        }

        return $filename;
    }

    /**
     * Check if such filename exists
     *
     * @param string $filename
     * @param int $folderId
     * @param int|null $id
     * @return bool
     */
    protected function isFileNameExists(string $filename, int $folderId, int $id = null): bool
    {
        $query = File::query()->where([['name', $filename], ['folder_id', $folderId]]);

        if (!is_null($id)) {
            $query->where('id', '!=', $id);
        }

        return $query->exists();
    }
}
