<?php
/**
 * Created by PhpStorm.
 * User: hellenmicky
 * Date: 20.06.2019
 * Time: 11:23
 */

namespace App\Services\Auth;

use Auth;

/**
 * Class LoginService
 * @package App\Services\Auth
 */
class LoginService
{
    /**
     * @var string
     */
    const ADMIN_GUARD = 'admin';

    /**
     * LoginService constructor.
     */
    public function __construct()
    {
        Auth::shouldUse(self::ADMIN_GUARD);
    }

    /**
     * Login
     *
     * @param array $credentials
     * @return boolean|string
     */
    public function login(array $credentials)
    {
        return Auth::attempt($credentials);
    }

    /**
     * Refresh expired token
     *
     * @return string
     */
    public function refreshToken(): string
    {
        return Auth::refresh();
    }

    /**
     * Logout
     *
     * @return void
     */
    public function logout(): void
    {
        Auth::invalidate();
    }
}
