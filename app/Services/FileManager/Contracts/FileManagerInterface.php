<?php
/**
 * Created by PhpStorm.
 * User: manowartop
 * Date: 2019-07-15
 * Time: 19:00
 */

namespace App\Services\FileManager\Contracts;

use Illuminate\Http\UploadedFile;

/**
 * File manager common interface(storage/GCP/AWS...)
 *
 * Interface FileManagerInterface
 * @package App\Services\FileManager\Contracts
 */
interface FileManagerInterface
{
    /**
     * Get file info (size/dimensions/extension/name)
     *
     * @param UploadedFile $file
     * @return array
     */
    public function getFileInfo(UploadedFile $file): array;

    /**
     * Upload file and get path
     *
     * @param string $destinationPath
     * @param UploadedFile $file
     * @return mixed
     */
    public function uploadFileAndGetPath(string $destinationPath, UploadedFile $file): ?string;

    /**
     * Move file and get new path
     *
     * @param string $oldPath
     * @param string $newPath
     * @param string $extension
     * @return null|string
     * @throws FileNotFoundException
     */
    public function moveFileAndGetPath(string $oldPath, string $newPath, string $extension): ?string;

    /**
     * Delete file
     *
     * @param string $path
     * @return bool
     */
    public function deleteFile(string $path): bool;
}
