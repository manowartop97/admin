<?php
/**
 * Created by PhpStorm.
 * User: manowartop
 * Date: 2019-07-15
 * Time: 18:53
 */

namespace App\Services\FileManager\Contracts;

use App\Models\FileManager\File;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Interface FileServiceInterface
 * @package App\Services\FileManager\Contracts
 */
interface FileServiceInterface
{
    /**
     * Get files by search params(folder is required)
     *
     * @param array $search
     * @return Collection
     */
    public function getFiles(array $search): Collection;

    /**
     * Create file
     *
     * @param array $data
     * @return Model|null
     */
    public function createFile(array $data): ?Model;

    /**
     * Rename file
     *
     * @param File|Model $file
     * @param string $name
     * @return Model|null
     */
    public function renameFile(Model $file, string $name): ?Model;

    /**
     * Move file
     *
     * @param array $fileIdsToMove
     * @param int $folderId
     * @return bool
     */
    public function moveFiles(array $fileIdsToMove, int $folderId): bool;

    /**
     * Delete files by ids
     *
     * @param array $fileId
     * @return bool
     */
    public function deleteFiles(array $fileId): bool;
}
