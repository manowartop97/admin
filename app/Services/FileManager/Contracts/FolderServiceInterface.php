<?php
/**
 * Created by PhpStorm.
 * User: hellenmicky
 * Date: 11.07.2019
 * Time: 12:05
 */

namespace App\Services\FileManager\Contracts;

use App\Exceptions\FileManager\UnableToCreateFolderException;
use App\Models\FileManager\Folder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Interface FolderServiceInterface
 * @package App\Services\FileManager\Contracts
 */
interface FolderServiceInterface
{
    /**
     * Get all directories
     *
     * @return Collection
     * @throws Exception
     */
    public function getAll(): Collection;

    /**
     * Get root directories
     *
     * @return Collection
     * @throws Exception
     */
    public function getRootFolders(): Collection;

    /**
     * Create folder entity
     *
     * @param array $data
     * @return Model|null
     * @throws UnableToCreateFolderException
     */
    public function createFolder(array $data): ?Model;

    /**
     * Update folder entity
     *
     * @param Folder $folder
     * @param array $data
     * @return Model|null
     * @throws Exception
     */
    public function updateFolder(Folder $folder, array $data): ?Model;

    /**
     * Delete folder and children
     *
     * @param Folder $folder
     * @return bool
     * @throws Exception
     */
    public function deleteFolder(Folder $folder): bool;
}