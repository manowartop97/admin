<?php
/**
 * Created by PhpStorm.
 * User: hellenmicky
 * Date: 11.07.2019
 * Time: 12:09
 */

namespace App\Services\FileManager\Contracts;

use App\Exceptions\FileManager\FolderNotFoundException;
use App\Models\FileManager\Folder;

/**
 * Interface StorageFolderServiceInterface
 * @package App\Services\FileManager\Contracts
 */
interface StorageFolderServiceInterface
{
    /**
     * Create folder
     *
     * @param Folder $folder
     * @return bool
     */
    public function createRealFolderForVirtual(Folder $folder): bool;

    /**
     * Move folder with all children
     *
     * @param string $oldFolderPath
     * @param Folder $folder
     * @return bool
     * @throws FolderNotFoundException
     */
    public function moveRealFolderOfVirtual(string $oldFolderPath, Folder $folder): bool;

    /**
     * Delete folder
     *
     * @param Folder $folder
     * @return bool
     */
    public function deleteRealFolderOfVirtual(Folder $folder): bool;

    /**
     * Get full path to currently created folder
     *
     * @param Folder $folder
     * @return string
     */
    public function getFullPathToFolder(Folder $folder): string;
}
