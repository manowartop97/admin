<?php
/**
 * Created by PhpStorm.
 * User: manowartop
 * Date: 2019-07-15
 * Time: 18:52
 */

namespace App\Services\FileManager;

use App\Exceptions\FileManager\UnableToMoveFileException;
use App\Exceptions\FileManager\UnableToUploadFileException;
use App\Models\FileManager\File;
use App\Models\FileManager\Folder;
use App\Repositories\FileManager\Contracts\FileRepositoryInterface;
use App\Services\FileManager\Contracts\FileManagerInterface;
use App\Services\FileManager\Contracts\FileServiceInterface;
use App\Services\FileManager\Contracts\StorageFolderServiceInterface;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class FileService
 * @package App\Services\FileManager
 */
class FileService implements FileServiceInterface
{
    /**
     * @var FileRepositoryInterface
     */
    protected $fileRepository;

    /**
     * @var FileManagerInterface
     */
    protected $fileManager;

    /**
     * @var StorageFolderServiceInterface
     */
    protected $storageFolderService;

    /**
     * FileService constructor.
     * @param FileRepositoryInterface $fileRepository
     * @param FileManagerInterface $fileManager
     * @param StorageFolderServiceInterface $storageFolderService
     */
    public function __construct(
        FileRepositoryInterface $fileRepository,
        FileManagerInterface $fileManager,
        StorageFolderServiceInterface $storageFolderService
    )
    {
        $this->fileRepository = $fileRepository;
        $this->fileManager = $fileManager;
        $this->storageFolderService = $storageFolderService;
    }

    /**
     * Get files by search params(folder is required)
     *
     * @param array $search
     * @return Collection
     */
    public function getFiles(array $search): Collection
    {
        return $this->fileRepository->getFilteredCollection($search);
    }

    /**
     * Create file and upload to the storage
     *
     * @param array $data
     * @return Model|null
     * @throws Exception
     */
    public function createFile(array $data): ?Model
    {
        /** @var Folder $folder */
        $folder = Folder::query()->findOrFail($data['folder_id']);
        $data = array_merge($data, $this->fileManager->getFileInfo($data['file']));

        if (is_null($filePath = $this->fileManager->uploadFileAndGetPath(
            $this->storageFolderService->getFullPathToFolder($folder),
            $data['file'])
        )) {
            throw new UnableToUploadFileException();
        }

        $data['path'] = $filePath;

        return $this->fileRepository->create($data);
    }

    /**
     * Rename file
     *
     * @param File|Model $file
     * @param string $name
     * @return Model|null
     */
    public function renameFile(Model $file, string $name): ?Model
    {
        return $this->fileRepository->update($file, ['name' => $name]);
    }

    /**
     * Move file
     *
     * @param array $fileIdsToMove
     * @param int $folderId
     * @return bool
     * @throws UnableToMoveFileException
     */
    public function moveFiles(array $fileIdsToMove, int $folderId): bool
    {
        foreach ($this->fileRepository->getFilteredCollection(['id' => $fileIdsToMove])->all() as $file) {
            if (is_null($this->moveFile($file, $folderId))) {
                return false;
            }
        }

        return true;
    }

    /**
     * Delete files by ids
     *
     * @param array $fileIds
     * @return bool
     * @throws Exception
     */
    public function deleteFiles(array $fileIds): bool
    {
        foreach ($this->fileRepository->getFilteredCollection(['id' => $fileIds])->all() as $file) {
            if (!$this->deleteFile($file)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Delete file
     *
     * @param Model|File $file
     * @return bool
     * @throws Exception
     */
    protected function deleteFile(Model $file): bool
    {
        if (!$this->fileRepository->delete($file)) {
            return false;
        }

        return $this->fileManager->deleteFile($file->path);
    }

    /**
     * Move file
     *
     * @param File|Model $file
     * @param int $folderId
     * @return Model|null
     * @throws UnableToMoveFileException
     */
    protected function moveFile(Model $file, int $folderId): ?Model
    {
        if ($file->folder_id === $folderId) {
            throw new UnableToMoveFileException();
        }

        /**
         * @var Folder $destinationFolder
         */
        $destinationFolder = Folder::query()->findOrFail($folderId);

        if (is_null($newPath = $this->fileManager->moveFileAndGetPath(
            $file->path,
            $this->storageFolderService->getFullPathToFolder($destinationFolder),
            $file->extension)
        )) {
            return null;
        }

        return $this->fileRepository->update($file, [
            'path'      => $newPath,
            'folder_id' => $folderId
        ]);
    }
}
