<?php
/**
 * Created by PhpStorm.
 * User: manowartop
 * Date: 2019-07-15
 * Time: 19:00
 */

namespace App\Services\FileManager;

use App\Exceptions\FileManager\FileNotFoundException;
use App\Services\FileManager\Contracts\FileManagerInterface;
use Carbon\Carbon;
use Illuminate\Filesystem\FilesystemAdapter;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Str;
use Storage;

/**
 * Class StorageFileManager
 * @package App\Services\FileManager\Contracts
 */
class StorageFileManager implements FileManagerInterface
{
    /**
     * @var FilesystemAdapter
     */
    protected $storage;

    /**
     * StorageFileManager constructor.
     * @param string $disk
     */
    public function __construct(string $disk = 'public')
    {
        $this->storage = Storage::disk($disk);
    }

    /**
     * Get file info (size/dimensions/extension/name)
     *
     * @param UploadedFile $file
     * @return array
     */
    public function getFileInfo(UploadedFile $file): array
    {
        $data = [
            'name'      => $file->getClientOriginalName(),
            'extension' => $file->getClientOriginalExtension(),
            'size'      => $file->getSize(),
        ];

        if (preg_match('/image/ui', $file->getMimeType())) {
            $dimensions = getimagesize($file->path());
            $width = $dimensions[0] ?? 0;
            $height = $dimensions[1] ?? 0;

            $data['dimensions'] = "{$width}x{$height}";
        }

        return $data;
    }

    /**
     * Upload file and get path
     *
     * @param string $destinationPath
     * @param UploadedFile $file
     * @return mixed
     */
    public function uploadFileAndGetPath(string $destinationPath, UploadedFile $file): ?string
    {
        $filename = $this->generateUniqueFileName($file->getClientOriginalExtension());

        while ($this->storage->exists($destinationPath . '/' . $filename)) {
            $filename = $this->generateUniqueFileName($file->getClientOriginalExtension());
        }

        if($this->storage->putFileAs($destinationPath, $file, $filename)=== false){
            return null;
        }

        return $destinationPath . '/' . $filename;
    }

    /**
     * Move file and get new path
     *
     * @param string $oldPath
     * @param string $newPath
     * @param string $extension
     * @return null|string
     * @throws FileNotFoundException
     */
    public function moveFileAndGetPath(string $oldPath, string $newPath, string $extension): ?string
    {
        if (!$this->storage->exists($oldPath)) {
            throw new FileNotFoundException();
        }

        $newName = $this->generateUniqueFileName($extension);

        if (!$this->storage->move($oldPath, $newPath . '/' . $newName)) {
            return null;
        }

        return $newPath . '/' . $newName;
    }

    /**
     * Delete file
     *
     * @param string $path
     * @return bool
     */
    public function deleteFile(string $path): bool
    {
        if ($this->storage->exists($path)) {
            return $this->storage->delete($path);
        }

        return true;
    }

    /**
     * Get extension from file name
     *
     * @param string $path
     * @return string
     */
    protected function getFileExtensionFromPath(string $path): string
    {
        $explodedPath = explode('/', $path);
        $fileName = $explodedPath[count($explodedPath) - 1];

        return explode('.', $fileName)[1] ?? '';
    }

    /**
     * Generate unique file name
     *
     * @param string $extension
     * @return string
     */
    protected function generateUniqueFileName(string $extension): string
    {
        return Str::random(3) . Carbon::now()->format('dmYHi') . '.' . $extension;
    }
}
