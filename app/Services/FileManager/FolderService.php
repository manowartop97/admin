<?php
/**
 * Created by PhpStorm.
 * User: manowartop
 * Date: 2019-07-09
 * Time: 22:12
 */

namespace App\Services\FileManager;

use App\Exceptions\FileManager\UnableToCreateFolderException;
use App\Exceptions\FileManager\UnableToDeleteFolderException;
use App\Exceptions\FileManager\UnableToMoveFolderException;
use App\Models\FileManager\Folder;
use App\Repositories\FileManager\Contracts\FolderRepositoryInterface;
use App\Repositories\FileManager\FolderRepository;
use App\Services\FileManager\Contracts\FolderServiceInterface;
use App\Services\FileManager\Contracts\StorageFolderServiceInterface;
use DB;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

/**
 * Class FolderService
 * @package App\Services\FileManager
 */
class FolderService implements FolderServiceInterface
{
    /**
     * @var FolderRepository|FolderRepository
     */
    protected $folderRepository;

    /**
     * @var int
     */
    protected $folderNameLength = 5;

    /**
     * @var StorageFolderService
     */
    protected $storageFolderService;

    /**
     * FolderService constructor.
     * @param FolderRepositoryInterface $folderRepository
     * @param StorageFolderServiceInterface $storageFolderService
     */
    public function __construct(FolderRepositoryInterface $folderRepository, StorageFolderServiceInterface $storageFolderService)
    {
        $this->folderRepository = $folderRepository;
        $this->storageFolderService = $storageFolderService;
    }

    /**
     * Get root directories
     *
     * @return Collection
     * @throws Exception
     */
    public function getRootFolders(): Collection
    {
        return Folder::whereIsRoot()->defaultOrder()->get();
    }

    /**
     * Get all directories
     *
     * @return Collection
     * @throws Exception
     */
    public function getAll(): Collection
    {
        return $this->folderRepository->getFilteredCollection();
    }

    /**
     * Create folder entity
     *
     * @param array $data
     * @return Model|null
     * @throws UnableToCreateFolderException
     */
    public function createFolder(array $data): ?Model
    {
        $data['hash'] = $this->generateUniqueFolderName($data['parent_id'] ?? 0);

        /**
         * @var Folder $folder
         */
        if (is_null($folder = $this->folderRepository->create($data))) {
            return null;
        }

        if (!$this->storageFolderService->createRealFolderForVirtual($folder)) {
            throw new UnableToCreateFolderException();
        }

        return $folder;
    }

    /**
     * Update folder entity
     *
     * @param Folder $folder
     * @param array $data
     * @return Model|null
     * @throws Exception
     */
    public function updateFolder(Folder $folder, array $data): ?Model
    {
        /**
         * @var Folder $folder
         */
        DB::beginTransaction();

        $isFolderParentChanged = $this->isFolderParentChanged($folder, $data['parent_id'] ?? null);

        $data['hash'] = $isFolderParentChanged
            ? $this->generateUniqueFolderName($data['parent_id'] ?? null)
            : $folder->hash;

        $oldFolderPath = $this->storageFolderService->getFullPathToFolder($folder);

        // Update folder model
        if (is_null($folder = $this->folderRepository->update($folder, $data))) {
            DB::rollBack();
            return null;
        }

        // Move folder and children to other destination
        if ($isFolderParentChanged && !$this->storageFolderService->moveRealFolderOfVirtual($oldFolderPath, $folder)) {
            DB::rollBack();
            throw new UnableToMoveFolderException();
        }

        DB::commit();
        return $folder;
    }

    /**
     * Delete folder and children
     *
     * @param Folder $folder
     * @return bool
     * @throws Exception
     */
    public function deleteFolder(Folder $folder): bool
    {
        DB::beginTransaction();

        // Removing all children
        $folder->descendants()->delete();

        if (!$this->storageFolderService->deleteRealFolderOfVirtual($folder)) {
            throw new UnableToDeleteFolderException();
        }

        if (!$this->folderRepository->delete($folder)) {
            DB::rollBack();
            return false;
        }

        DB::commit();
        return true;
    }

    /**
     * Has folder parent changed
     *
     * @param Folder $folder
     * @param int|null $parentId
     * @return bool
     */
    protected function isFolderParentChanged(Folder $folder, int $parentId = null): bool
    {
        return $parentId !== $folder->parent_id;
    }

    /**
     * Generating unique folder name
     *
     * @param int|null $parentId
     * @return string
     */
    protected function generateUniqueFolderName(int $parentId = null): string
    {
        $name = Str::substr(md5(Str::random($this->folderNameLength)), 0, $this->folderNameLength);

        while (Folder::query()->where([['hash', $name], ['parent_id', $parentId]])->exists()) {
            $name = Str::substr(md5(Str::random($this->folderNameLength)), 0, $this->folderNameLength);
        }

        return $name;
    }
}
