<?php
/**
 * Created by PhpStorm.
 * User: hellenmicky
 * Date: 10.07.2019
 * Time: 13:18
 */

namespace App\Services\FileManager;

use App\Exceptions\FileManager\FolderNotFoundException;
use App\Models\FileManager\Folder;
use App\Services\FileManager\Contracts\StorageFolderServiceInterface;
use Illuminate\Filesystem\FilesystemAdapter;
use Illuminate\Support\Facades\Storage;

/**
 * Class StorageFolderService
 * @package App\Services\FileManager
 */
class StorageFolderService implements StorageFolderServiceInterface
{
    /**
     * @var FilesystemAdapter
     */
    protected $storage;

    /**
     * StorageFolderService constructor.
     * @param string $disk
     */
    public function __construct(string $disk = 'public')
    {
        $this->storage = Storage::disk($disk);
    }

    /**
     * Create folder
     *
     * @param Folder $folder
     * @return bool
     */
    public function createRealFolderForVirtual(Folder $folder): bool
    {
        $folderPath = $this->getFullPathToFolder($folder);

        if (!$this->storage->exists($folderPath)) {
            return $this->storage->makeDirectory($folderPath);
        }

        return true;
    }

    /**
     * Move folder with all children
     *
     * @param string $oldFolderPath
     * @param Folder $folder
     * @return bool
     * @throws FolderNotFoundException
     */
    public function moveRealFolderOfVirtual(string $oldFolderPath, Folder $folder): bool
    {
        if (!$this->storage->exists($oldFolderPath)) {
            throw new FolderNotFoundException();
        }

        return $this->storage->move($oldFolderPath, $this->getFullPathToFolder($folder));
    }

    /**
     * Delete folder
     *
     * @param Folder $folder
     * @return bool
     */
    public function deleteRealFolderOfVirtual(Folder $folder): bool
    {
        $folderPath = $this->getFullPathToFolder($folder);

        if ($this->storage->exists($folderPath)) {
            return $this->storage->deleteDirectory($folderPath);
        }

        return true;
    }

    /**
     * Get full path to currently created folder
     *
     * @param Folder $folder
     * @return string
     */
    public function getFullPathToFolder(Folder $folder): string
    {
        $parentsFolders = array_column($folder->ancestors()->select('hash')->get()->toArray(), 'hash');

        $path = empty($parentsFolders) ? '' : implode('/', $parentsFolders);

        return $path . '/' . $folder->hash;
    }
}
