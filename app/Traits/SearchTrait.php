<?php
/**
 * Created by PhpStorm.
 * User: manowartop
 * Date: 2019-06-22
 * Time: 20:53
 */

namespace App\Traits;

use Exception;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class SearchTrait
 * @package App\Traits
 *
 * @property string $entityClass
 * @property array $filterableParams
 * @property array $orders
 */
trait SearchTrait
{
    /**
     * Get filtered collection
     *
     * @param array $search
     * @return Collection
     * @throws Exception
     */
    public function getFilteredCollection(array $search = []): Collection
    {
        return $this->getFilteredQuery($search)->get();
    }

    /**
     * Get filtered and paginated result
     *
     * @param array $search
     * @param int $pageSize
     * @return LengthAwarePaginator
     * @throws Exception
     */
    public function getFilteredAndPaginatedResults(array $search = [], int $pageSize = 15): LengthAwarePaginator
    {
        return $this->getFilteredQuery($search)->paginate($pageSize);
    }

    /**
     * Get filtered query
     *
     * @param array $search
     * @return Builder
     * @throws Exception
     */
    protected function getFilteredQuery(array $search = []): Builder
    {
        if (!property_exists(static::class, 'filterableParams')) {
            throw new Exception("Filterable params must be declared");
        }

        $query = $this->getResolvedEntity()::query();

        foreach ($search as $paramName => $paramValue) {
            if (!isset($this->filterableParams[$paramName]) || is_null($paramValue) || empty($paramValue)) {
                continue;
            }

            switch ($this->filterableParams[$paramName]['operator']) {
                case 'null':
                    $query->whereNull($paramName, $paramValue);
                    break;
                case 'like':
                    $query->where($paramName, $this->filterableParams[$paramName]['operator'], "%$paramValue%");
                    break;
                case 'in':
                    $query->whereIn($paramName, $paramValue);
                    break;
                default:
                    $query->where($paramName, $this->filterableParams[$paramName]['operator'], "$paramValue");
                    break;
            }
        }

        if (property_exists(static::class, 'orders') && !empty($this->orders)) {
            foreach ($this->orders as $param => $direction) {
                $query->orderBy($param, $direction);
            }
        }

        return $query;
    }

    /**
     * Get entity of child
     *
     * @return Model
     * @throws Exception
     */
    protected function getResolvedEntity(): Model
    {
        if (!property_exists(self::class, 'entityClass')) {
            throw new Exception("Entity class can not be null");
        }

        return resolve($this->entityClass);
    }
}
