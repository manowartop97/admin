<?php

namespace App\Models\Admin;

use Illuminate\Foundation\Auth\User;
use Tymon\JWTAuth\Contracts\JWTSubject;

/**
 * @property integer $id
 * @property string $email
 * @property string $password
 * @property string $remember_token
 * @property string $created_at
 * @property string $updated_at
 */
class Admin extends User implements JWTSubject
{
    /**
     * @var string
     */
    protected $guard = 'admin';

    /**
     * @var array
     */
    protected $fillable = ['email', 'created_at', 'updated_at'];

    /**
     * @var array
     */
    protected $hidden = ['password', 'remember_token',];

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
}
