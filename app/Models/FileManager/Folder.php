<?php

namespace App\Models\FileManager;

use App\Models\Admin\Admin;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Kalnoy\Nestedset\NodeTrait;

/**
 * @property int $id
 * @property int $admin_id
 * @property string $name
 * @property string $hash
 * @property int $_lft
 * @property int $_rgt
 * @property int $parent_id
 * @property boolean $is_favourite
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Admin $admin
 * @property File[] $files
 *
 * @method static self whereIsRoot()
 * @method Builder defaultOrder()
 */
class Folder extends Model
{
    use NodeTrait;

    /**
     * @var array
     */
    protected $fillable = ['admin_id', 'name', 'hash', '_lft', '_rgt', 'parent_id', 'is_favourite', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function files()
    {
        return $this->hasMany(File::class);
    }
}
