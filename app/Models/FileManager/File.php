<?php

namespace App\Models\FileManager;

use App\Models\Admin\Admin;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

/**
 * @property int $id
 * @property int $admin_id
 * @property int $folder_id
 * @property int $size
 * @property string $name
 * @property string $path
 * @property string $extension
 * @property string $dimensions
 * @property boolean $is_favourite
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Admin $admin
 * @property Folder $folder
 */
class File extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['admin_id', 'folder_id', 'size', 'name', 'path', 'extension', 'dimensions', 'is_favourite', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function folder()
    {
        return $this->belongsTo(Folder::class);
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return asset('/storage/'.$this->path);
    }
}
