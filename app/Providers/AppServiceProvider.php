<?php

namespace App\Providers;

use App\Api\V1\Serializers\NoDataArraySerializer;
use App\Models\FileManager\File;
use App\Models\FileManager\Folder;
use App\Observers\FileManager\FileObserver;
use App\Observers\FileManager\FolderObserver;
use App\Repositories\FileManager\Contracts\FileRepositoryInterface;
use App\Repositories\FileManager\Contracts\FolderRepositoryInterface;
use App\Repositories\FileManager\FileRepository;
use App\Repositories\FileManager\FolderRepository;
use App\Services\FileManager\Contracts\FileManagerInterface;
use App\Services\FileManager\Contracts\FileServiceInterface;
use App\Services\FileManager\Contracts\FolderServiceInterface;
use App\Services\FileManager\Contracts\StorageFolderServiceInterface;
use App\Services\FileManager\FileService;
use App\Services\FileManager\FolderService;
use App\Services\FileManager\StorageFileManager;
use App\Services\FileManager\StorageFolderService;
use Dingo\Api\Transformer\Adapter\Fractal;
use Dingo\Api\Transformer\Factory;
use Illuminate\Support\ServiceProvider;
use League\Flysystem\Filesystem;
use League\Fractal\Manager;

/**
 * Class AppServiceProvider
 * @package App\Providers
 */
class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Observers
        Folder::observe(FolderObserver::class);
        File::observe(FileObserver::class);
        // End Observers

        // Services
        $this->app->singleton(FolderServiceInterface::class, FolderService::class);
        $this->app->singleton(StorageFolderServiceInterface::class, StorageFolderService::class);
        $this->app->singleton(FileManagerInterface::class, StorageFileManager::class);
        $this->app->singleton(FileServiceInterface::class, FileService::class);
        // EndServices

        // Repositories
        $this->app->singleton(FolderRepositoryInterface::class, FolderRepository::class);
        $this->app->singleton(FileRepositoryInterface::class, FileRepository::class);
        // End Repositories

        app()[Factory::class]->setAdapter(function () {
            return new Fractal((new Manager())->setSerializer(new NoDataArraySerializer()));
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
