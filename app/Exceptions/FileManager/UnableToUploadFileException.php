<?php
/**
 * Created by PhpStorm.
 * User: manowartop
 * Date: 2019-07-15
 * Time: 19:52
 */

namespace App\Exceptions\FileManager;

use Exception;
use Throwable;

/**
 * Class UnableToUploadFileException
 * @package App\Exceptions\FileManager
 */
class UnableToUploadFileException extends Exception
{
    /**
     * UnableToUploadFileException constructor.
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct(string $message = "Unable to upload file", int $code = 400, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
