<?php
/**
 * Created by PhpStorm.
 * User: hellenmicky
 * Date: 17.07.2019
 * Time: 12:19
 */

namespace App\Exceptions\FileManager;

use Exception;
use Throwable;

/**
 * Class UnableToMoveFileException
 * @package App\Exceptions\FileManager
 */
class UnableToMoveFileException extends Exception
{
    /**
     * UnableToMoveFileException constructor.
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct(string $message = "Не удалось переместить файл", int $code = 400, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}