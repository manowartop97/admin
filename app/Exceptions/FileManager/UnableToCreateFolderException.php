<?php
/**
 * Created by PhpStorm.
 * User: hellenmicky
 * Date: 10.07.2019
 * Time: 13:36
 */

namespace App\Exceptions\FileManager;

use Exception;
use Throwable;

/**
 * Class UnableToCreateFolderException
 * @package App\Exceptions\FileManager
 */
class UnableToCreateFolderException extends Exception
{
    /**
     * UnableToCreateFolderException constructor.
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct(string $message = "Error while creating directory", int $code = 400, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}