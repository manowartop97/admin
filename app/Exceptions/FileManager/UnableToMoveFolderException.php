<?php
/**
 * Created by PhpStorm.
 * User: hellenmicky
 * Date: 11.07.2019
 * Time: 11:50
 */

namespace App\Exceptions\FileManager;

use Exception;
use Throwable;

/**
 * Class UnableToMoveFolderException
 * @package App\Exceptions\FileManager
 */
class UnableToMoveFolderException extends Exception
{
    /**
     * UnableToMoveFolderException constructor.
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct(string $message = "Unable to move folder", int $code = 400, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}