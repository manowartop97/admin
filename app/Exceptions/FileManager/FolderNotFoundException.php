<?php
/**
 * Created by PhpStorm.
 * User: hellenmicky
 * Date: 10.07.2019
 * Time: 15:30
 */

namespace App\Exceptions\FileManager;

use Exception;
use Throwable;

/**
 * Class FolderNotFoundException
 * @package App\Exceptions\FileManager
 */
class FolderNotFoundException extends Exception
{
    /**
     * FolderNotFoundException constructor.
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct(string $message = "Folder not found", int $code = 404, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}