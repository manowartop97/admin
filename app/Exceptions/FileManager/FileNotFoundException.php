<?php
/**
 * Created by PhpStorm.
 * User: hellenmicky
 * Date: 17.07.2019
 * Time: 13:54
 */

namespace App\Exceptions\FileManager;

use Exception;
use Throwable;

/**
 * Class FileNotFoundException
 * @package App\Exceptions\FileManager
 */
class FileNotFoundException extends Exception
{
    /**
     * FileNotFoundException constructor.
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct(string $message = "File not fount", int $code = 404, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}