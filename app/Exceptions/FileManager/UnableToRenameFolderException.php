<?php
/**
 * Created by PhpStorm.
 * User: manowartop
 * Date: 2019-07-10
 * Time: 18:26
 */

namespace App\Exceptions\FileManager;

use Exception;
use Throwable;

/**
 * Class UnableToRenameFolderException
 * @package App\Exceptions\FileManager
 */
class UnableToRenameFolderException extends Exception
{
    /**
     * UnableToRenameFolderException constructor.
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct(string $message = "Unable to rename folder", int $code = 400, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
