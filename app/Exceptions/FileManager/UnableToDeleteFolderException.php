<?php
/**
 * Created by PhpStorm.
 * User: hellenmicky
 * Date: 10.07.2019
 * Time: 13:40
 */

namespace App\Exceptions\FileManager;

use Exception;
use Throwable;

/**
 * Class UnableToDeleteFolderException
 * @package App\Exceptions\FileManager
 */
class UnableToDeleteFolderException extends Exception
{
    /**
     * UnableToDeleteFolderException constructor.
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct(string $message = "Error while deleting directory", int $code = 400, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}