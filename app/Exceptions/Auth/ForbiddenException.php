<?php
/**
 * Created by PhpStorm.
 * User: manowartop
 * Date: 2019-06-20
 * Time: 23:27
 */

namespace App\Exceptions\Auth;

use Exception;
use Throwable;

/**
 * Class ForbiddenException
 * @package App\Exceptions\Auth
 */
class ForbiddenException extends Exception
{
    /**
     * ForbiddenException constructor.
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct(string $message = "You are not allowed to perform this action", int $code = 403, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
