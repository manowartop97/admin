<?php
/**
 * Created by PhpStorm.
 * User: hellenmicky
 * Date: 20.06.2019
 * Time: 11:18
 */

namespace App\Api\V1\Http\Controllers\Auth;

use App\Api\V1\Http\Controllers\ApiController;
use App\Api\V1\Http\Requests\Auth\LoginRequest;
use App\Api\V1\Http\Transformers\Admin\AdminTransformer;
use App\Services\Auth\LoginService;
use Dingo\Api\Http\Response;
use JWTAuth;

/**
 * Class LoginController
 * @package App\Api\V1\Http\Controllers\Auth
 */
class LoginController extends ApiController
{
    /**
     * @var LoginService
     */
    private $loginService;

    /**
     * LoginController constructor.
     * @param LoginService $loginService
     */
    public function __construct(LoginService $loginService)
    {
        $this->loginService = $loginService;
    }

    /**
     * Get admin
     *
     * @return Response
     */
    public function getUser(): Response
    {
        return $this->response->item(JWTAuth::user(), new AdminTransformer(), ['key' => 'data']);
    }

    /**
     * Login
     *
     * @param LoginRequest $request
     * @return Response
     */
    public function login(LoginRequest $request): Response
    {
        if (!$token = $this->loginService->login($request->validated())) {
            $this->response->errorUnauthorized();
        }

        return Response::create([
            'data' => [
                'type'  => 'Bearer',
                'token' => $token
            ]
        ]);
    }

    /**
     * Refresh token
     *
     * @return Response
     */
    public function refresh(): Response
    {
        $token = $this->loginService->refreshToken();

        return Response::create([
            'data' => [
                'type'  => 'Bearer',
                'token' => $token
            ]
        ]);
    }

    /**
     * Logout
     *
     * @return void
     */
    public function logout(): void
    {
        $this->loginService->logout();
        $this->response->noContent();
    }
}
