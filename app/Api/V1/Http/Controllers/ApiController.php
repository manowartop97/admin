<?php
/**
 * Created by PhpStorm.
 * User: hellenmicky
 * Date: 20.06.2019
 * Time: 11:18
 */

namespace App\Api\V1\Http\Controllers;

use App\Http\Controllers\Controller;
use Dingo\Api\Routing\Helpers;

/**
 * Class ApiController
 * @package App\Api\V1\Http\Controllers
 */
class ApiController extends Controller
{
    use Helpers;
}