<?php
/**
 * Created by PhpStorm.
 * User: manowartop
 * Date: 2019-07-09
 * Time: 22:30
 */

namespace App\Api\V1\Http\Controllers\FileManager;

use App\Api\V1\Http\Controllers\ApiController;
use App\Api\V1\Http\Requests\FileManager\Folder\CreateFolderRequest;
use App\Api\V1\Http\Requests\FileManager\Folder\UpdateFolderRequest;
use App\Api\V1\Http\Transformers\FileManager\FolderTransformer;
use App\Exceptions\FileManager\UnableToCreateFolderException;
use App\Models\FileManager\Folder;
use App\Services\FileManager\Contracts\FolderServiceInterface;
use Dingo\Api\Http\Response;
use Exception;

/**
 * Class FolderController
 * @package App\Api\V1\Http\Controllers\FileManager
 */
class FolderController extends ApiController
{
    /**
     * @var FolderServiceInterface
     */
    private $folderService;

    /**
     * FolderController constructor.
     * @param FolderServiceInterface $folderService
     */
    public function __construct(FolderServiceInterface $folderService)
    {
        $this->folderService = $folderService;
    }

    /**
     * Get all folders with tree structure
     *
     * @return Response
     * @throws Exception
     */
    public function getTree(): Response
    {
        return $this->response->collection(
            $this->folderService->getRootFolders(),
            new FolderTransformer(FolderTransformer::MODE_TREE),
            ['key' => 'data']
        );
    }

    /**
     * Get all folders with plain structure
     *
     * @return Response
     * @throws Exception
     */
    public function index(): Response
    {
        return $this->response->collection(
            $this->folderService->getAll(),
            new FolderTransformer(FolderTransformer::MODE_ALL),
            ['key' => 'data']
        );
    }

    /**
     * Create folder
     *
     * @param CreateFolderRequest $request
     * @return Response
     * @throws UnableToCreateFolderException
     * @throws Exception
     */
    public function store(CreateFolderRequest $request): Response
    {
        if (is_null($folder = $this->folderService->createFolder($request->validated()))) {
            $this->response->errorBadRequest('Не удалось создать папку');
        }

        return $this->response->item($folder, new FolderTransformer(FolderTransformer::MODE_TREE), ['key' => 'data']);
    }

    /**
     * Update folder
     *
     * @param Folder $folder
     * @param UpdateFolderRequest $request
     * @return Response
     * @throws Exception
     */
    public function update(Folder $folder, UpdateFolderRequest $request): Response
    {
        if (is_null($folder = $this->folderService->updateFolder($folder, $request->validated()))) {
            $this->response->errorBadRequest('Не удалось обновить папку');
        }

        return $this->response->item($folder, new FolderTransformer(FolderTransformer::MODE_TREE), ['key' => 'data']);
    }

    /**
     * Delete folder
     *
     * @param Folder $folder
     * @return void
     * @throws Exception
     */
    public function destroy(Folder $folder): void
    {
        if (!$this->folderService->deleteFolder($folder)) {
            $this->response->errorBadRequest('Не удалось удалить папку');
        }

        $this->response->noContent()->setStatusCode(204);
    }
}
