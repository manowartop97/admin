<?php
/**
 * Created by PhpStorm.
 * User: hellenmicky
 * Date: 15.07.2019
 * Time: 16:21
 */

namespace App\Api\V1\Http\Controllers\FileManager;

use App\Api\V1\Http\Controllers\ApiController;
use App\Api\V1\Http\Requests\FileManager\File\DestroyFilesRequest;
use App\Api\V1\Http\Requests\FileManager\File\MoveFilesRequest;
use App\Api\V1\Http\Requests\FileManager\File\RenameFileRequest;
use App\Api\V1\Http\Requests\FileManager\File\SearchFileRequest;
use App\Api\V1\Http\Requests\FileManager\File\StoreFileRequest;
use App\Api\V1\Http\Transformers\FileManager\FileTransformer;
use App\Models\FileManager\File;
use App\Services\FileManager\Contracts\FileServiceInterface;
use Dingo\Api\Http\Response;

/**
 * Class FileController
 * @package App\Api\V1\Http\Controllers\FileManager
 */
class FileController extends ApiController
{
    /**
     * @var FileServiceInterface
     */
    private $fileService;

    /**
     * FileController constructor.
     * @param FileServiceInterface $fileService
     */
    public function __construct(FileServiceInterface $fileService)
    {
        $this->fileService = $fileService;
    }

    /**
     * Get files
     *
     * @param SearchFileRequest $request
     * @return Response
     */
    public function index(SearchFileRequest $request): Response
    {
        return $this->response->collection(
            $this->fileService->getFiles($request->validated()),
            new FileTransformer(),
            ['key' => 'data']
        );
    }

    /**
     * Create file
     *
     * @param StoreFileRequest $request
     * @return Response
     */
    public function create(StoreFileRequest $request): Response
    {
        if (is_null($file = $this->fileService->createFile($request->validated()))) {
            $this->response->errorBadRequest('Не удалось загрузить файл');
        }

        return $this->response->item($file, new FileTransformer(), ['key' => 'data']);
    }

    /**
     * Rename file
     *
     * @param File $file
     * @param RenameFileRequest $request
     * @return Response
     */
    public function rename(File $file, RenameFileRequest $request): Response
    {
        if (is_null($file = $this->fileService->renameFile($file, $request->get('name')))) {
            $this->response->errorBadRequest('Не удалось переименовать файл');
        }

        return $this->response->item($file, new FileTransformer(), ['key' => 'data']);
    }

    /**
     * Move file(s)
     *
     * @param MoveFilesRequest $request
     * @return void
     */
    public function move(MoveFilesRequest $request): void
    {
        if (is_null($file = $this->fileService->moveFiles($request->get('id'), $request->get('folder_id')))) {
            $this->response->errorBadRequest('Не удалось переместить файл(ы)');
        }

        $this->response->noContent()->setStatusCode(200);
    }

    /**
     * Delete file(s)
     *
     * @param DestroyFilesRequest $request
     * @return void
     */
    public function delete(DestroyFilesRequest $request): void
    {
        if (!$this->fileService->deleteFiles($request->get('id'))) {
            $this->response->errorBadRequest('Не удалось удалить файл');
        }

        $this->response->noContent()->setStatusCode(204);
    }
}
