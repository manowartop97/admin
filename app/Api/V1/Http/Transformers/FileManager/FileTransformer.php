<?php
/**
 * Created by PhpStorm.
 * User: manowartop
 * Date: 2019-07-15
 * Time: 19:18
 */

namespace App\Api\V1\Http\Transformers\FileManager;

use App\Models\FileManager\File;
use League\Fractal\TransformerAbstract;

/**
 * Class FileTransformer
 * @package App\Api\V1\Http\Transformers\FileManager
 */
class FileTransformer extends TransformerAbstract
{
    /**
     * @param File $file
     * @return array
     */
    public function transform(File $file): array
    {
        return [
            'id'        => $file->id,
            'name'      => $file->name,
            'path'      => $file->getPath(),
            'extension' => $file->extension,
            'size'      => $file->size,
            'folder_id' => $file->folder_id,
            'timestamp' => $file->created_at->timestamp
        ];
    }
}
