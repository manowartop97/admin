<?php
/**
 * Created by PhpStorm.
 * User: manowartop
 * Date: 2019-07-09
 * Time: 22:48
 */

namespace App\Api\V1\Http\Transformers\FileManager;

use App\Models\FileManager\Folder;
use Exception;
use League\Fractal\TransformerAbstract;

/**
 * Class FolderTransformer
 * @package App\Api\V1\Http\Transformers\FileManager
 */
class FolderTransformer extends TransformerAbstract
{
    /**
     * @const
     */
    const MODE_TREE = 1;
    const MODE_ALL = 2;

    /**
     * FolderTransformer constructor.
     * @param int $mode
     * @throws Exception
     */
    public function __construct(int $mode)
    {
        if (!in_array($mode, [self::MODE_ALL, self::MODE_TREE])) {
            throw new Exception("Invalid mode {$mode}");
        }

        if ($mode === self::MODE_TREE) {
            $this->defaultIncludes[] = 'childFolders';
        }
    }

    /**
     * @param Folder $folder
     * @return array
     */
    public function transform(Folder $folder): array
    {
        return [
            'id'           => $folder->id,
            'name'         => $folder->name,
            'hash'         => $folder->hash,
            'admin_id'     => $folder->admin_id,
            'parent_id'    => $folder->parent_id,
            'left'         => $folder->_lft,
            'right'        => $folder->_rgt,
            'is_favourite' => $folder->is_favourite,
            'created_at'   => $folder->created_at->format('d.m.Y H:i'),
            'updated_at'   => $folder->updated_at->format('d.m.Y H:i'),
        ];
    }

    /**
     * Include child folders to response
     *
     * @param Folder $folder
     * @return \League\Fractal\Resource\Collection
     * @throws Exception
     */
    public function includeChildFolders(Folder $folder)
    {
        return $this->collection($folder->children()->defaultOrder()->get(), new FolderTransformer(self::MODE_TREE));
    }
}
