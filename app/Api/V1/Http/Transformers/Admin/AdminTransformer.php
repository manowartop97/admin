<?php
/**
 * Created by PhpStorm.
 * User: manowartop
 * Date: 2019-07-09
 * Time: 21:38
 */

namespace App\Api\V1\Http\Transformers\Admin;

use App\Models\Admin\Admin;
use League\Fractal\TransformerAbstract;

/**
 * Class AdminTransformer
 * @package App\Api\V1\Http\Transformers\Admin
 */
class AdminTransformer extends TransformerAbstract
{
    /**
     * @param Admin $admin
     * @return array
     */
    public function transform(Admin $admin): array
    {
        return [
            'id'    => $admin->id,
            'email' => $admin->email,
        ];
    }
}
