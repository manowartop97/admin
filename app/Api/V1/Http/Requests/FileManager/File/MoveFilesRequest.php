<?php
/**
 * Created by PhpStorm.
 * User: hellenmicky
 * Date: 17.07.2019
 * Time: 12:16
 */

namespace App\Api\V1\Http\Requests\FileManager\File;

use App\Api\V1\Http\Requests\Request;

/**
 * Class MoveFileRequest
 * @package App\Api\V1\Http\Requests\FileManager\File
 */
class MoveFilesRequest extends Request
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'id'        => 'required|array',
            'id.*'      => 'required|integer|exists:files,id',
            'folder_id' => 'required|integer|exists:folders,id'
        ];
    }

    /**
     * @return array
     */
    public function messages(): array
    {
        return [

        ];
    }
}