<?php
/**
 * Created by PhpStorm.
 * User: hellenmicky
 * Date: 18.07.2019
 * Time: 16:11
 */

namespace App\Api\V1\Http\Requests\FileManager\File;

use App\Api\V1\Http\Requests\Request;

/**
 * Class DestroyFilesRequest
 * @package App\Api\V1\Http\Requests\FileManager\File
 */
class DestroyFilesRequest extends Request
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'id'        => 'required|array',
            'id.*'      => 'required|integer|exists:files,id',
        ];
    }

    /**
     * @return array
     */
    public function messages(): array
    {
        return [

        ];
    }
}