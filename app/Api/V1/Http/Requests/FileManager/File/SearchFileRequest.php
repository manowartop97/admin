<?php
/**
 * Created by PhpStorm.
 * User: hellenmicky
 * Date: 16.07.2019
 * Time: 11:03
 */

namespace App\Api\V1\Http\Requests\FileManager\File;

use App\Api\V1\Http\Requests\Request;

/**
 * Class SearchFileRequest
 * @package App\Api\V1\Http\Requests\FileManager
 */
class SearchFileRequest extends Request
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'folder_id' => 'required|integer|exists:folders,id'
        ];
    }

    /**
     * @return array
     */
    public function messages(): array
    {
        return [

        ];
    }
}