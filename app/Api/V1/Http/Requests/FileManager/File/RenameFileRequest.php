<?php
/**
 * Created by PhpStorm.
 * User: hellenmicky
 * Date: 17.07.2019
 * Time: 10:13
 */

namespace App\Api\V1\Http\Requests\FileManager\File;

use App\Api\V1\Http\Requests\Request;

/**
 * Class RenameFileRequest
 * @package App\Api\V1\Http\Requests\FileManager
 */
class RenameFileRequest extends Request
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'name' => 'required|string'
        ];
    }

    /**
     * @return array
     */
    public function messages(): array
    {
        return [

        ];
    }
}