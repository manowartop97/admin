<?php
/**
 * Created by PhpStorm.
 * User: hellenmicky
 * Date: 15.07.2019
 * Time: 16:43
 */

namespace App\Api\V1\Http\Requests\FileManager\File;

use App\Api\V1\Http\Requests\Request;

/**
 * Class StoreFileRequest
 * @package App\Api\V1\Http\Requests\FileManager
 */
class StoreFileRequest extends Request
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'file'      => 'required|file',
            'folder_id' => 'required|integer|exists:folders,id'
        ];
    }

    /**
     * @return array
     */
    public function messages(): array
    {
        return [

        ];
    }
}
