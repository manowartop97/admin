<?php
/**
 * Created by PhpStorm.
 * User: manowartop
 * Date: 2019-07-09
 * Time: 22:42
 */

namespace App\Api\V1\Http\Requests\FileManager\Folder;

use App\Api\V1\Http\Requests\Request;
use Illuminate\Validation\Rule;

/**
 * Class CreateFolderRequest
 * @package App\Api\V1\Http\Requests\FileManager
 */
class CreateFolderRequest extends Request
{
    /**
     * @return array
     */
    public function rules(): array
    {
        $parentId = $this->get('parent_id', null);

        return [
            'name'         => [
                'required',
                'string',
                is_null($parentId)
                    ? Rule::unique('folders')->whereNull('parent_id')
                    : Rule::unique('folders')->where('parent_id', $parentId)
            ],
            'order'        => 'nullable|integer',
            'parent_id'    => 'nullable|integer',
            'is_favourite' => 'nullable|bool'
        ];
    }

    /**
     * @return array
     */
    public function messages(): array
    {
        return [

        ];
    }
}
