<?php
/**
 * Created by PhpStorm.
 * User: hellenmicky
 * Date: 10.07.2019
 * Time: 13:48
 */

namespace App\Api\V1\Http\Requests\FileManager\Folder;

use App\Api\V1\Http\Requests\Request;
use Illuminate\Validation\Rule;

/**
 * Class UpdateFolderRequest
 * @package App\Api\V1\Http\Requests\FileManager
 */
class UpdateFolderRequest extends Request
{
    /**
     * @return array
     */
    public function rules(): array
    {
        $parentId = $this->get('parent_id', null);

        return [
            'name'         => [
                'required',
                'string',
                is_null($parentId)
                    ? Rule::unique('folders')->whereNull('parent_id')->ignore($this->route('folder')->id)
                    : Rule::unique('folders')->where('parent_id', $parentId)->ignore($this->route('folder')->id)
            ],
            'order'        => 'nullable|integer',
            'parent_id'    => 'nullable|integer',
            'is_favourite' => 'nullable|bool'
        ];
    }

    /**
     * @return array
     */
    public function messages(): array
    {
        return [

        ];
    }
}
