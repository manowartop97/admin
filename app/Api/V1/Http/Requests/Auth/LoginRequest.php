<?php
/**
 * Created by PhpStorm.
 * User: hellenmicky
 * Date: 20.06.2019
 * Time: 11:23
 */

namespace App\Api\V1\Http\Requests\Auth;

use App\Api\V1\Http\Requests\Request;

/**
 * Class LoginRequest
 * @package App\Api\V1\Http\Requests\Auth
 */
class LoginRequest extends Request
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'email'    => 'required|email',
            'password' => 'required|string'
        ];
    }

    /**
     * @return array
     */
    public function messages(): array
    {
        return [

        ];
    }
}