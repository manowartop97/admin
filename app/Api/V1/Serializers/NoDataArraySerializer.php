<?php
/**
 * Created by PhpStorm.
 * User: hellenmicky
 * Date: 21.06.2019
 * Time: 16:49
 */

namespace App\Api\V1\Serializers;

use League\Fractal\Serializer\ArraySerializer;

/**
 * Class NoDataArraySerializer
 * @package App\Api\V1\Serializers
 */
class NoDataArraySerializer extends ArraySerializer
{
    /**
     * Serialize a collection.
     * @param $resourceKey
     * @param array $data
     * @return array
     */
    public function collection($resourceKey, array $data)
    {
        return ($resourceKey) ? [ $resourceKey => $data ] : $data;
    }

    /**
     * Serialize an item.
     * @param $resourceKey
     * @param array $data
     * @return array
     */
    public function item($resourceKey, array $data)
    {
        return ($resourceKey) ? [ $resourceKey => $data ] : $data;
    }
}